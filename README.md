Sutty Unbound Role
==================

Installs a caching local DNS resolver with resiliency options.  Prevents
resolution errors when nodes are down.

Example Playbook
----------------

```yaml
---
- hosts: "sutty"
  strategy: "free"
  remote_user: "root"
  tasks:
  - name: "unbound"
    include_role:
      name: "sutty_unbound"
    vars: {}
```

License
-------

MIT-Antifa
